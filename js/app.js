function showHideDescription () {
    $(this).find(".description").toggle();
}

function calculPrix () {
    var prix = 0;

    prix += $("input[name='type']:checked").data("price");
    prix += $("input[name='pate']:checked").data("price");
    $("input[name='extra']:checked").each(function (i, elem) {
        prix += $(elem).data('price');
    });
    var part = $(".nb-parts input").val();
    prix = prix * part / 6;
    prix = Math.round(prix * 100) / 100;

    $('.stick-right p').text(prix + "€");
}

$(document).ready(function () {
    //Aff ingrédients des pizzas
    $('.pizza-type label').hover(
        showHideDescription,
        showHideDescription
    );

    //Calcul prix
    $('.type input[data-price]').change(calculPrix);

    //Aff nb de part de pizza
    $(".nb-parts input").change(function () {
        $(".pizza-pict").remove();
        const pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();

        for(i = 0; i < slices/6; i++){
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 !== 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);

        calculPrix();
    });

    //Etape Suivante
    $('.next-step').click(function () {
        $(this).remove();
        $('.infos-client').show();
    });

    //Ajouter adresse
    $('.add').click(function () {
       $("<br/><input type=\'text\'/>").insertBefore(this);
    });

    //Commande
    $('.done').click(function () {

        var name;

        name = $('#nameformessage').val();

        $(".main").empty();
        //alert("Merci PRENOM ! Votre commande sera livrée dans 15 minutes.");
        $('.test').text("Merci " + name + " ! Votre commande sera livrée dans 15 minutes.")
    })

    //Prix
});